package br.com.proway.trabalho0107.controller;

import br.com.proway.trabalho0107.model.SerVivo;
import br.com.proway.trabalho0107.repository.SerVivoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class SerVivoController {
    @Autowired
    private SerVivoRepository serVivoRepository;

    @GetMapping("/seresvivos")
    public ResponseEntity<List<SerVivo>> getAllSerVivo() {
        try {
            return new ResponseEntity<>(serVivoRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/ser-vivo/add")
    public ResponseEntity<SerVivo> addSerVivo(@RequestBody SerVivo serVivo) {
        try {
            serVivo = serVivoRepository.save(serVivo);
            return new ResponseEntity<>(serVivo, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
