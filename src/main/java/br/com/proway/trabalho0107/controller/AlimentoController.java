package br.com.proway.trabalho0107.controller;

import br.com.proway.trabalho0107.model.Alimento;
import br.com.proway.trabalho0107.repository.AlimentoRepository;
import br.com.proway.trabalho0107.repository.SerVivoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AlimentoController {

    @Autowired
    private AlimentoRepository alimentoRepository;
    @Autowired
    private SerVivoRepository serVivoRepository;

    @GetMapping("/alimentos")
    public ResponseEntity<List<Alimento>> getAlimentos() {
        try {
            return new ResponseEntity<>(alimentoRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/alimento/add/{idSerVivo}")
    public ResponseEntity<Alimento> addAlimento(@RequestBody Alimento alimento, @PathVariable("idSerVivo") long idSerVivo) {
        try {
            alimento.setSerVivo(serVivoRepository.findById(idSerVivo));
            alimento = alimentoRepository.save(alimento);
            return new ResponseEntity<>(alimento, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
