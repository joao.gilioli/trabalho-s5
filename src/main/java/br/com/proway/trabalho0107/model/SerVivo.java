package br.com.proway.trabalho0107.model;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@Table(name = "ser_vivo")
@EntityListeners(AuditingEntityListener.class)
public class SerVivo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "especie")
    private String especie;
}
