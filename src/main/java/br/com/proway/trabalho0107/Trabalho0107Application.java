package br.com.proway.trabalho0107;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Trabalho0107Application {

	public static void main(String[] args) {
		SpringApplication.run(Trabalho0107Application.class, args);
	}

}
