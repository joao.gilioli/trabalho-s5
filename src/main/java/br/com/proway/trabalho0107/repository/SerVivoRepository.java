package br.com.proway.trabalho0107.repository;

import br.com.proway.trabalho0107.model.SerVivo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SerVivoRepository extends JpaRepository<SerVivo, Long> {

    List<SerVivo> findAll();

    SerVivo findById(long id);

    SerVivo save(SerVivo serVivo);

}
