package br.com.proway.trabalho0107.repository;

import br.com.proway.trabalho0107.model.Alimento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlimentoRepository extends JpaRepository<Alimento, Long> {

    List<Alimento> findAll();

    Alimento findById(long id);

    Alimento save(Alimento alimento);
}
